# Written by Ben Keitch 2015
# Copyright: Free to use so long as the author is attributed
#
# Open a Windows 7 sticky note file, and extracts the notes as RTF files
# these can be read into a word processor or use 'unrtf' to decode
import olefile
assert olefile.isOleFile('StickyNotes.snt')
ole = olefile.OleFileIO('StickyNotes.snt')
names = ole.listdir(True, True);
# magic number for RTF
magic = bytearray("{\\rtf", "ascii")
for storage in names:
  st_type = ole.get_type(storage)
  if st_type == olefile.STGTY_STREAM:
    note = ole.openstream(storage)
    data = note.read()
    dab = bytearray(data)
    if dab[:5] == magic:
      print("Found : " +storage[0]+'.rtf')
      notefile = open(storage[0]+'.rtf', 'wb')
      notefile.write(data)
      notefile.close()
ole.close()
