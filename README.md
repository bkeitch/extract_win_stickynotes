Windows Stick Notes Reader
==========================

Author: Ben Keitch
2015-02

The purpose of this Python script is to extract the data from Windows Sticky Notes.

These can be converted to other formats using unrtf.

Usage
-----

Copy the file:

StickyNotes.snt into the working folder

Run the pyton script:

    python extract_notes.py  

The shell script will then convert these to HTML using unrtf
